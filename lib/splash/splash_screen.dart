import 'package:flutter/material.dart';
import 'package:skillarthi/image_constants.dart';
import 'package:get/get.dart';
import 'package:skillarthi/main.dart';
import 'package:skillarthi/web_view/webview.dart';
import 'package:skillarthi/welcome/welcom_screen.dart';
import 'splash_view_model.dart';
import 'package:get_storage/get_storage.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super();

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final timeStorage = GetStorage();
  @override
  void initState() {
    Future.delayed(const Duration(seconds: 3), () {
      // Get.off(const MyHomePage(title: 'MyHome'));
      if (timeStorage.read('appUsedTime') != null) {
        String appUsedTime = timeStorage.read('appUsedTime');
        DateTime theTime = DateTime.parse(appUsedTime);
        if (DateTime.now().difference(theTime).inDays >= 3) {
          Get.off(() => const WebViewScreen());
        } else {
          String timeStamp = DateTime.now().toIso8601String();
          timeStorage.write('appUsedTime', timeStamp);
          Get.off(() => const WelcomeScreen());
        }
      } else {
        String timeStamp = DateTime.now().toIso8601String();
        timeStorage.write('appUsedTime', timeStamp);
        Get.off(() => const WelcomeScreen());
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GetBuilder<SplashScreenViewModel>(
        init: SplashScreenViewModel(),
        builder: (controller) {
          return Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    stops: [0.1, 0.9],
                    begin: Alignment.topLeft,
                    colors: [
                      Color.fromRGBO(125, 115, 193, 1),
                      Color.fromRGBO(86, 74, 168, 1)
                    ])),
            child: Stack(
              fit: StackFit.expand,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      ImageConstants.splashLogo,
                    ),
                    Image.asset(ImageConstants.splashText)
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
