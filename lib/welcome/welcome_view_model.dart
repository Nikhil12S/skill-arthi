import 'package:get/get.dart';
import 'package:skillarthi/image_constants.dart';


class WelcomeController extends GetxController {

  var counter = 0.obs;

  void increment() => counter.value++;

  List<PageModel> pageModel = [
    PageModel(ImageConstants.onboardOne,   'Skill Learning', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit enim, ac amet ultrices.'),
    PageModel(ImageConstants.onboardTwo,   'Mentoring', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit enim, ac amet ultrices.'),
    PageModel(ImageConstants.onboardThree, 'Community of teens', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sit enim, ac amet ultrices.'),
  ];
  var currentPageValue = 0.obs;


}










class PageModel{
  String imageName;
  String text;
  String bodyText;
  PageModel(this.imageName, this.text, this.bodyText);
}