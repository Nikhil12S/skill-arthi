import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:skillarthi/image_constants.dart';
import 'package:skillarthi/web_view/webview.dart';
import 'package:skillarthi/welcome/welcome_view_model.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  List<PageModel> pageModel = [];
  int currentPageValue = 0;
  @override
  void initState() {
    pageModel = [
      PageModel(ImageConstants.onboardOne, 'Skill Learning',
          '"You dont learn to walk  by following rules. You learn by doing, and by falling over.”   - Richard Branso'),
      PageModel(ImageConstants.onboardTwo, 'Mentoring',
          '"My job is not to be easy on people. My job is to take these great people we have and to push them and make them even better." - Steve Jobs'),
      PageModel(ImageConstants.onboardThree, 'Community of teens',
          '“The richest people in the world look for and build networks, everyone else looks for work. Marinate on that for a minute."- Robert Kiyosaki.'),
    ];
    super.initState();
  }

  Widget circleBar(bool isActive) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 8),
      height: isActive ? 8 : 4,
      width: isActive ? 8 : 4,
      decoration: BoxDecoration(
          color: isActive
              ? const Color.fromRGBO(234, 98, 171, 1.0)
              : const Color.fromRGBO(234, 98, 171, 0.3),
          borderRadius: const BorderRadius.all(Radius.circular(12))),
    );
  }

  Widget pageWidget(WelcomeController controller) {
    return Obx(() {
      return Stack(
        alignment: AlignmentDirectional.topStart,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 35),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                for (int i = 0; i < pageModel.length; i++)
                  if (i == controller.currentPageValue.value) ...[
                    circleBar(true)
                  ] else
                    circleBar(false),
              ],
            ),
          ),
        ],
      );
    });
  }

  Widget pageViewNavigationWidget(WelcomeController controller) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        const Text(
          'Skip',
          style: TextStyle(
              color: Color(0xffEA62AB),
              fontSize: 16.0,
              fontWeight: FontWeight.bold),
        ),
        IconButton(
          iconSize: 62.0,
          onPressed: () {
            if (controller.currentPageValue.value == 0) {
              controller.currentPageValue.value = 1;
              pageController.jumpTo(
                1.0,
              );
            } else if (controller.currentPageValue.value == 1) {
              controller.currentPageValue.value = 2;
              pageController.jumpTo(
                2.0,
              );
            } else if (controller.currentPageValue.value == 2) {
              Get.off(const WebViewScreen());
            }
          },
          icon: Image.asset(ImageConstants.forwardCircle),
        ),
      ],
    );
  }

  PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xffDBE4EC),
        body: GetBuilder<WelcomeController>(
            init: WelcomeController(),
            builder: (value) {
              return Padding(
                padding: const EdgeInsets.all(32.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: PageView.builder(
                        physics: const ClampingScrollPhysics(),
                        itemCount: value.pageModel.length,
                        onPageChanged: (int page) {
                          value.currentPageValue.value = page;
                        },
                        controller: pageController,
                        itemBuilder: (context, index) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Obx(() {
                                return Image.asset(value
                                    .pageModel[value.currentPageValue.value]
                                    .imageName);
                              }),
                              const SizedBox(
                                height: 50,
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                    Obx(() => Text(
                          value.pageModel[value.currentPageValue.value].text,
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24.0),
                        )),
                    const SizedBox(
                      height: 12,
                    ),
                    Obx(() {
                      return Text(
                        pageModel[value.currentPageValue.value].bodyText,
                        style: const TextStyle(
                            fontWeight: FontWeight.normal, fontSize: 14.0),
                      );
                    }),
                    const SizedBox(
                      height: 50,
                    ),
                    pageWidget(value),
                    const SizedBox(
                      height: 50,
                    ),
                    pageViewNavigationWidget(value)
                  ],
                ),
              );
            }));
  }
}
