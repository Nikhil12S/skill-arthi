

class ImageConstants{
  static const String onboardOne = 'assets/images/onboard1.png';
  static const String onboardTwo = 'assets/images/onboard2.png';
  static const String onboardThree = 'assets/images/onboard3.png';
  static const String splashLogo = 'assets/images/splash_logo.png';
  static const String splashText = 'assets/images/splash_text.png';
  static const String forwardCircle = 'assets/images/circle.png';
}